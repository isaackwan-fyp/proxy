var express = require('express');
var app = express();
require('express-async-errors');
const puppeteer = require('puppeteer');
const imagemin = require('imagemin');
const imageminPlugin = require('imagemin-pngquant')({
	quality: "20-40",
});

app.locals.browser = puppeteer.launch();

app.get("/screenshot", async function screenshotHandler(req, res) {
	if (!req.query.width) {
		throw new Error("Width required");
	}

	if (!req.query.url) {
		throw new Error("URL required");
	}

	const browser = await app.locals.browser;

	const page = await browser.newPage();
	await page.setViewport({width: parseInt(req.query.width), height: 10});
	await page.goto(req.query.url, {waitUntil: "load"});
	const screenshot = await page.screenshot({
		fullPage: true,
	});

	const compressed = await imagemin.buffer(screenshot, {}, [imageminPlugin]);
	res.type("png");
	res.send(compressed);
});

app.listen(process.env.PORT || 4201, function() {
	console.log("Ready");
});